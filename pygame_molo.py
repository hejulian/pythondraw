import pygame
from pygame.locals import *
import sys, os
import time
import numpy as np


def draw_leds(led_locations):
    for i, (x,y) in enumerate(led_locations):
        x += np.abs(led_locations_minx)+20
        y += np.abs(led_locations_miny)+20
        leds[i] = dict()
        leds[i]["circle"] = pygame.draw.circle(screen, GRAY, (x, y), led_size)
        leds[i]["touched"] = False
        leds[i]["time"] = time.time()

def button(msg,x,y,w,h,ic,ac):
    mouse = pygame.mouse.get_pos()

    if x+w > mouse[0] > x and y+h > mouse[1] > y:
        pygame.draw.rect(gameDisplay, ac,(x,y,w,h))
    else:
        pygame.draw.rect(gameDisplay, ic,(x,y,w,h))

    smallText = pygame.font.Font(None ,20)
    textSurf, textRect = text_objects(msg, smallText)
    textRect.center = ( (x+(w/2)), (y+(h/2)) )
    gameDisplay.blit(textSurf, textRect)

def text_objects(text, font, color):
    textSurface = font.render(text, True, color)
    return textSurface, textSurface.get_rect()


pygame.init()
pygame.display.set_caption('Paint your Molo')
background_colour = (255,255,255)
(width, height) = (500, 500)
clear_btn_size = (width-100,height-100, 80, 30)

BLACK = pygame.Color( 0 ,  0 ,  0 )
GRAY = pygame.Color(100, 100, 100)
WHITE = pygame.Color(255, 255, 255)
RED = pygame.Color(255, 0, 0)
RED_DARK = pygame.Color(150, 0, 0)

mouse = pygame.mouse
fpsClock = pygame.time.Clock()



screen = pygame.display.set_mode((width, height))
canvas = screen.copy()
screen.fill(background_colour)


pygame.display.flip()

running = True
# canvas.fill(WHITE)


led_locations = np.load("led_locations.npy")*210
led_locations_minx = np.min(led_locations[:, 0])
led_locations_miny = np.min(led_locations[:, 1])
led_size = 5
leds = dict()
screen.fill(BLACK)

clear_btn = pygame.draw.rect(screen, RED, clear_btn_size)
smallText = pygame.font.Font(None,20)
textSurf, textRect = text_objects("clear", smallText, BLACK)
textRect.center = ( (height-100+80/2), (width-100+30/2) )
screen.blit(textSurf, textRect)



draw_leds(led_locations)
while True:
    for event in pygame.event.get():
        if event.type == QUIT:
            pygame.quit()
            sys.exit()

        mouse = pygame.mouse.get_pos()
        leftclick, middleclick, rightclick = pygame.mouse.get_pressed()
        if leftclick:
            # if width-100+50 > mouse[0] > width-100 and height-100+20 > mouse[1] > height-100:
            if clear_btn.collidepoint(mouse):
                clear_btn = pygame.draw.rect(screen, RED_DARK, clear_btn_size)
                draw_leds(led_locations)
                screen.blit(textSurf, textRect)
        else:
            clear_btn = pygame.draw.rect(screen, RED, clear_btn_size)
            screen.blit(textSurf, textRect)

        for l in leds:
            # if (leds[l].x - pygame.mouse.get_pos()[0] < 1E-2) and  (leds[l].y - pygame.mouse.get_pos()[1] < 1E-2):
            if leds[l]["circle"].collidepoint(mouse):
                if leds[l]["touched"] and (np.abs(time.time() - leds[l]["time"]) > 0.5):
                    leds[l]["circle"] = pygame.draw.circle(screen, GRAY, leds[l]["circle"].center, led_size)
                    leds[l]["touched"] = False
                    leds[l]["time"] = time.time()
                elif not leds[l]["touched"] and (np.abs(time.time() - leds[l]["time"]) > 0.5):
                    leds[l]["circle"] = pygame.draw.circle(screen, RED, leds[l]["circle"].center, led_size)
                    leds[l]["touched"] = True
                    leds[l]["time"] = time.time()
                screen.blit(canvas, (0, 0))
    # clear_screen(led_locations)

    # pygame.draw.circle(screen, BLACK, (pygame.mouse.get_pos()), 5)
    pygame.display.update()
