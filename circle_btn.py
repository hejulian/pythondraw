# importing libraries
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
import numpy as np
import sys


class Window(QMainWindow):
    def __init__(self):
        super().__init__()

        # setting title
        self.setWindowTitle("Python ")

        # setting geometry
        self.setGeometry(100, 100, 600, 400)

        # calling method
        self.UiComponents()
        self.coordinates = np.load("led_locations.npy")
        self.set_array = np.zeros(self.coordinates.shape[0])
        # showing all the widgets
        self.show()

    # method for widgets
    def UiComponents(self):
        btn_list = list()
        coordinates = np.load("led_locations.npy")
        scale_f = 250
        minx, miny = (np.abs(np.min(coordinates[:, 0]))*scale_f, np.abs(np.min(coordinates[:, 1]))*scale_f)
        for i, (x, y) in enumerate(coordinates):
        # creating a push button
            button = QPushButton("", self)
            # setting geometry of button

            button.setGeometry(minx+x*scale_f, miny+y*scale_f, 10, 10)
            button.setCheckable(True)
            button.setObjectName(f"{i}")
            # setting radius and border
            button.setStyleSheet("border-radius : 5; border : 1px solid black; background-color : lightgrey")

            # adding action to a button
            button.clicked.connect(self.clickme)

    # action method
    def clickme(self):
        btn = self.sender()
        i = int(btn.objectName())
        if btn.isChecked():
            self.set_array[i] = 1
            # setting background color to light-blue
            btn.setStyleSheet("border-radius :5; border : 1px solid black;background-color : red")

        # if it is unchecked
        else:
            self.set_array[i] = 0
            # set background color back to light-grey
            btn.setStyleSheet("border-radius : 5; border : 1px solid black;background-color : lightgrey")
        # printing pressed

        print("pressed", str(i))

# create pyqt5 app
App = QApplication(sys.argv)

# create the instance of our Window
window = Window()

# start the app
sys.exit(App.exec())
