import sys
from PyQt5 import QtCore, QtGui, QtWidgets, uic
from PyQt5.QtCore import Qt

from PyQt5.QtCore    import Qt
from PyQt5.QtGui     import QPixmap, QPainter, QPainterPath
from PyQt5.QtWidgets import QLabel, QWidget, QHBoxLayout, QApplication
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm

class Canvas(QtWidgets.QLabel):


    def __init__(self,  *args, antialiasing=True, **kwargs):
        super(Canvas, self).__init__(*args, **kwargs)

        self.radius = 25

        self.width = 200
        self.height = 200

        self.target = QPixmap(self.width, self.height)
        self.target.fill(Qt.GlobalColor.transparent)

        p = QPixmap("mask.png")

        painter = QPainter(self.target)


        path = QPainterPath()
        path.addRect(0, 0, self.width, self.height)

        painter.setClipPath(path)
        painter.drawPixmap(0, 0, p)
        self.setPixmap(self.target)

        #
        # self.width = 200
        # self.height = 200
        # pixmap = QtGui.QPixmap(self.width, self.height)
        # # pixmap = pixmap.scaled(self.width*5, self.height*5)
        # self.setPixmap(pixmap)
        # self.clear_screen()
        self.last_x, self.last_y = None, None
        self.pen_color = QtGui.QColor('#a42f3b')




    def set_pen_color(self, c):
        print(c)
        self.pen_color = QtGui.QColor(c)

    def clear_screen(self):
        empty = np.zeros((self.width, self.height))
        self.pixmap().fill(Qt.GlobalColor.transparent)
        p = QPixmap("mask.png")

        painter = QPainter(self.target)


        path = QPainterPath()
        path.addRect(0, 0, self.width, self.height)

        painter.setClipPath(path)
        painter.drawPixmap(0, 0, p)
        self.setPixmap(self.target)

        self.update()
        self.report_pixels()


    def report_pixels(self):
        channels_count = 4
        w = int(self.width)
        h = int(self.height)
        image = self.pixmap().scaled(w, h).toImage()
        s = image.bits().asstring(w * h * channels_count)
        arr = np.frombuffer(s, dtype=np.uint8).reshape((h, w, channels_count))
        img = 1-np.int64(np.all(arr[:, :, :3] == 0, axis=2))
        # img = img.reshape()
        plt.imsave("image.png", img, cmap=cm.gray)
        # img.show()
        # print(int(np.clip(x, 0, 600)/600*28), int(np.clip(y, 0, 600)/600*28))

    def mouseMoveEvent(self, e):
        if self.last_x is None: # First event.
            self.last_x = e.x()
            self.last_y = e.y()
            return # Ignore the first time.

        painter = QtGui.QPainter(self.pixmap())
        painter.setCompositionMode(QtGui.QPainter.CompositionMode_DestinationOver)
        p = painter.pen()
        p.setWidth(10)
        p.setColor(self.pen_color)
        painter.setPen(p)
        painter.drawLine(self.last_x, self.last_y, e.x(), e.y())

        self.report_pixels()

        painter.end()
        self.update()

        # Update the origin for next time.
        self.last_x = e.x()
        self.last_y = e.y()

    def mouseReleaseEvent(self, e):
        self.last_x = None
        self.last_y = None

# COLORS = ['#a42f3b'
# 17 undertones https://lospec.com/palette-list/17undertones
# '#000000', '#141923', '#414168', '#3a7fa7', '#35e3e3', '#8fd970', '#5ebb49',
# '#458352', '#dcd37b', '#fffee5', '#ffd035', '#cc9245', '#a15c3e', '#a42f3b',
# '#f45b7a', '#c24998', '#81588d', '#bcb0c2', '#ffffff',
# ]


class QPaletteButton(QtWidgets.QPushButton):

    def __init__(self, color):
        super().__init__()
        self.setFixedSize(QtCore.QSize(24,24))
        self.color = color
        self.setStyleSheet("background-color: %s;" % color)


class MainWindow(QtWidgets.QMainWindow):

    def __init__(self):
        super().__init__()

        self.canvas = Canvas()

        w = QtWidgets.QWidget()
        l = QtWidgets.QVBoxLayout()
        w.setLayout(l)
        l.addWidget(self.canvas)

        palette = QtWidgets.QHBoxLayout()
        # self.add_palette_buttons(palette)
        self.add_buttons(palette)
        l.addLayout(palette)

        self.setCentralWidget(w)
        # self.showFullScreen()

    def button_pressed(self):
        print("Button Pressed")

    def add_buttons(self, layout):
        btn = QtWidgets.QPushButton("Clear")
        btn.clicked.connect(self.canvas.clear_screen)
        layout.addWidget(btn)

    def add_palette_buttons(self, layout):
        for i, c in enumerate(COLORS):
            b = QPaletteButton(COLORS[i])
            b.clicked.connect(lambda state, c=c: self.canvas.set_pen_color(c))
            layout.addWidget(b)


app = QtWidgets.QApplication(sys.argv)
window = MainWindow()
window.show()
app.exec_()
